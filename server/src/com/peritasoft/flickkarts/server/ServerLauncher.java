package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.peritasoft.flickkarts.FlickKarts;

public class ServerLauncher {
    public static void main(String[] args) {
        final FlickKarts game = new FlickKarts(new FlickKarts.ScreenBuilder() {
            @Override
            public Screen build(FlickKarts game) {
                return new ServerScreen();
            }
        });
        game.setLoadAssets(false);
        new HeadlessApplication(game);
    }
}
