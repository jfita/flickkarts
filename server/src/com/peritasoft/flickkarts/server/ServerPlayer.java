package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.utils.Json;
import com.peritasoft.flickkarts.Player;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerPlayer extends Player {
    private static final Logger log = LoggerFactory.getLogger(ServerPlayer.class);
    private final WebSocket conn;
    private final Json serializer;
    private ServerRace race;
    private String raceCode;

    public ServerPlayer(int id, final WebSocket conn, Json serializer) {
        super(id, null, null);
        if (conn == null) {
            throw new IllegalArgumentException("server player connection can not be null");
        }
        if (serializer == null) {
            throw new IllegalArgumentException("server player serializer can not be null");
        }
        this.conn = conn;
        this.serializer = serializer;
    }

    public ServerRace getRace() {
        return race;
    }

    public void setRace(ServerRace race) {
        this.race = race;
    }

    public void setRaceCode(String raceCode) {
        this.raceCode = raceCode;
    }

    public String getRaceCode() {
        return raceCode;
    }

    public void send(Object msg) {
        conn.send(serialize(msg));
    }

    private String serialize(Object object) {
        log.debug("Send to {}: {}", this, object);
        return serializer.toJson(object, Object.class);
    }
}