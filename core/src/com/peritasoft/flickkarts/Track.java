package com.peritasoft.flickkarts;

public class Track {
    String name;
    String trackMap;
    String trackTexture;
    int laps;
    int difficulty;

    public Track() {
        this("undefined", null, null, 0, 0);
    }

    public Track(String name, String trackMap, String trackTexture, int laps, int difficulty) {
        this.name = name;
        this.trackMap = trackMap;
        this.trackTexture = trackTexture;
        this.laps = laps;
        this.difficulty = difficulty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrackMap() {
        return trackMap;
    }

    public void setTrackMap(String trackMap) {
        this.trackMap = trackMap;
    }

    public String getTrackTexture() {
        return trackTexture;
    }

    public void setTrackTexture(String trackTexture) {
        this.trackTexture = trackTexture;
    }

    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}
