package com.peritasoft.flickkarts;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Itinerary {
    private final Vector2[] itinerary;
    private final Checkpoint[] checkpoints;

    public Itinerary(Vector2[] itinerary, Checkpoint[] checkpoints) {
        this.itinerary = itinerary;
        this.checkpoints = checkpoints;
    }

    public Checkpoint[] getCheckpoints() {
        return checkpoints;
    }

    public int findClosestNode(Vector2 position) {
        float minDistance = Float.MAX_VALUE;
        int nodeIdx = -1;
        for (int i = 0; i < itinerary.length; i++) {
            Vector2 node = itinerary[i];
            float nodeDistance = position.dst2(node);
            if (nodeDistance < minDistance) {
                minDistance = nodeDistance;
                nodeIdx = i;
            }
        }
        return nodeIdx;
    }

    public float distanceToNode(Vector2 position, int node) {
        return position.dst2(itinerary[node]);
    }

    public Vector2 getDirection(int node) {
        int nextNode = (node + 1) % itinerary.length;
        Vector2 direction = new Vector2(itinerary[nextNode]);
        direction.sub(itinerary[node]);
        return direction;
    }

    public static class Checkpoint {
        public final int number;
        public final Rectangle rectangle;

        public Checkpoint(int number, Rectangle rectangle) {
            this.number = number;
            this.rectangle = rectangle;
        }
    }
}
