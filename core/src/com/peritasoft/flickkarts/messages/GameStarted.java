package com.peritasoft.flickkarts.messages;

import com.peritasoft.flickkarts.KartColor;
import com.peritasoft.flickkarts.Race;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("FieldMayBeFinal")
public class GameStarted {
    private Race.Parameters raceParameters;
    private Map<String, String> players;
    private Map<String, KartColor> playersColor;
    private ArrayList<KartState> kartsState;

    @SuppressWarnings("unused")
    public GameStarted() {
        this(null, null, null, null);
    }

    public GameStarted(
            final Race.Parameters raceParameters
            , final Map<String, String> players
            , final Map<String, KartColor> playersColor
            , final ArrayList<KartState> kartState
    ) {
        this.raceParameters = raceParameters;
        this.players = players;
        this.playersColor = playersColor;
        this.kartsState = kartState;
    }

    public Map<String, String> getPlayers() {
        return players;
    }

    public List<KartState> getKartsState() {
        return kartsState;
    }

    public Map<String, KartColor> getPlayersColor() {
        return playersColor;
    }

    public Race.Parameters getRaceParameters() {
        return raceParameters;
    }

    @Override
    public String toString() {
        return "GameStarted{" +
                "players=" + players +
                ", playersColor=" + playersColor +
                ", kartsState=" + kartsState +
                ", raceParameters=" + raceParameters +
                '}';
    }
}