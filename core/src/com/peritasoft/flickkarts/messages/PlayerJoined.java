package com.peritasoft.flickkarts.messages;

import com.peritasoft.flickkarts.KartColor;

@SuppressWarnings("FieldMayBeFinal")
public class PlayerJoined {
    private int playerId;
    private String playerName;
    private KartColor kartColor;

    @SuppressWarnings("unused")
    public PlayerJoined() {
        this(-1, "Ghost", KartColor.BLACK);
    }

    public PlayerJoined(int playerId, String playerName, KartColor kartColor) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.kartColor = kartColor;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public KartColor getKartColor() {
        return kartColor;
    }

    @Override
    public String toString() {
        return "PlayerJoined{" +
                "playerId=" + playerId +
                ", playerName='" + playerName + '\'' +
                ", kartColor='" + kartColor + '\'' +
                '}';
    }
}
