package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class ChangeRaceLaps {
    private int laps;

    @SuppressWarnings("unused")
    public ChangeRaceLaps() {
        this(3);
    }

    public ChangeRaceLaps(int laps) {
        this.laps = laps;
    }

    public int getLaps() {
        return laps;
    }

    @Override
    public String toString() {
        return "ChangeRaceLaps{" +
                "laps=" + laps +
                '}';
    }
}
