package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class PlayerLeftRace {
    private int playerId;

    @SuppressWarnings("unused")
    public PlayerLeftRace() {
        this(-1);
    }

    public PlayerLeftRace(int playerId) {
        this.playerId = playerId;
    }

    public int getPlayerId() {
        return playerId;
    }

    @Override
    public String toString() {
        return "PlayerLeftRace{" +
                "playerId=" + playerId +
                '}';
    }
}
