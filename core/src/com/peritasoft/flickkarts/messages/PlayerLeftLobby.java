package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class PlayerLeftLobby {
    private int playerId;

    @SuppressWarnings("unused")
    public PlayerLeftLobby() {
        this(-1);
    }

    public PlayerLeftLobby(int playerId) {
        this.playerId = playerId;
    }

    public int getPlayerId() {
        return playerId;
    }

    @Override
    public String toString() {
        return "PlayerLeftLobby{" +
                "playerId=" + playerId +
                '}';
    }
}
