package com.peritasoft.flickkarts;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.peritasoft.flickkarts.items.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kart {
    public static final float RADIUS = 0.03f;
    private static final Logger log = LoggerFactory.getLogger(Kart.class);

    private final Body body;
    private final Player player;
    private int lap;
    private int currentRound;
    private int currentSector;
    private Item item;
    private boolean burst;

    public Kart(float x, float y, Player player, World world) {
        body = createKart(x, y, world);
        this.player = player;
        this.lap = 0;
        currentRound = 0;
        currentSector = 0;
        item = null;
        burst = false;
    }

    private Body createKart(float posX, float posY, World world) {
        BodyDef def = new BodyDef();
        def.position.set(posX, posY);
        def.type = BodyDef.BodyType.DynamicBody;
        Body body = world.createBody(def);
        body.setLinearDamping(2.5f);
        body.setAngularDamping(2.5f);
        body.setUserData(this);
        FixtureDef fixture = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(RADIUS);
        fixture.shape = shape;
        fixture.density = 1f;
        fixture.restitution = 0.2f;
        body.createFixture(fixture);
        shape.dispose();
        return body;
    }

    public int getId() {
        return player.getId();
    }

    public String getPlayerName() {
        return player.getName();
    }

    public KartColor getPlayerColor() {
        return player.getColor();
    }

    public Item getItem() {
        return item;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public boolean itsYours(Fixture fixture) {
        return fixture.getBody() == body;
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    public float getAngle() {
        return body.getAngle() * MathUtils.radiansToDegrees;
    }

    public int getLap() {
        return lap;
    }

    public void flick(Vector2 impulse) {
        log.debug("flick {} impulse: {}", getId(), impulse);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public boolean isMoving() {
        return body.isAwake();
    }

    public void adjust(Vector2 position, float angle, int sector, int lap) {
        log.debug("adjust position {} : {} to {} : {}", body.getPosition(), body.getAngle(), position, angle);
        log.debug("adjust sector {} lap {}", sector, lap);
        body.setTransform(position, angle * MathUtils.degreesToRadians);
        this.currentSector = sector;
        this.lap = lap;
    }

    public void crossFinishLine() {
        currentSector = 0;
        lap++;
    }

    public Vector2 getDirection() {
        return body.getLinearVelocity();
    }

    public void dispose() {
        body.getWorld().destroyBody(body);
    }

    public int getCurrentSector() {
        return currentSector;
    }

    public void nextSector() {
        currentSector++;
        log.debug("Kart {} is now at sector {}", getId(), currentSector);
    }

    public void addFriction(float friction) {
        body.setLinearDamping(body.getLinearDamping() + friction);
    }

    public void subFriction(float friction) {
        body.setLinearDamping(body.getLinearDamping() - friction);
    }

    public void pickItem(Item item) {
        System.out.println("Kart " + getId() + " picks up an item");
        this.item = item;
    }

    public void useItem() {
        if (item != null) {
            item.use(this);
            item = null;
        }
    }

    public boolean hasBurst() {
        return burst;
    }

    public void setBurst() {
        burst = true;
    }

    public void removeBurst() {
        burst = false;
    }
}