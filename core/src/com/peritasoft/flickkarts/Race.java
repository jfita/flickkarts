package com.peritasoft.flickkarts;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.flickkarts.items.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Race implements QueryCallback, Disposable {
    private static final Logger log = LoggerFactory.getLogger(Race.class);
    private final World world;
    private final ArrayList<Kart> karts;
    private final ArrayList<Kart> kartsToClassify;
    private final ArrayList<Kart> kartsClassified;
    private final ArrayList<ItemBox> itemBoxes;
    private final ArrayList<ItemBox> itemBoxesToDelete;
    private final Vector2 queryTouchPoint;
    private final int laps;
    private final TiledMap map;
    private final Rectangle boundaryBox;
    private Itinerary itinerary;
    private boolean isOverKart;
    private Kart queryKart;
    private Kart currentKart;
    private int currentRound;
    private Listener listener;
    private SfxPlayer sfxPlayer;

    public Race(final Parameters parameters, final Iterable<? extends Player> players, TmxMapLoader loader, Listener listener, SfxPlayer sfxPlayer) {
        this.world = new World(Vector2.Zero, true);
        this.itinerary = null;
        this.sfxPlayer = sfxPlayer;
        this.world.setContactListener(new RaceContactListener(sfxPlayer));
        this.karts = new ArrayList<Kart>();
        this.kartsToClassify = new ArrayList<Kart>();
        this.kartsClassified = new ArrayList<Kart>();
        this.itemBoxes = new ArrayList<ItemBox>();
        this.itemBoxesToDelete = new ArrayList<ItemBox>();
        queryTouchPoint = new Vector2();
        currentRound = 1;
        this.laps = parameters.laps;
        this.listener = listener;

        map = loader.load(parameters.track);
        TiledObjectUtil.parseTiledObjectLayer(world, map.getLayers().get("collision").getObjects());
        boundaryBox = getMapBoundaryBox();
        createBoundary();
        Rectangle finishLine = TiledObjectUtil.getRectangle(map.getLayers().get("start").getObjects(), "finishLine");
        TiledObjectUtil.scaleRectangle(finishLine, FlickKarts.UNIT_SCALE);
        setFinishLine(finishLine);
        this.itinerary = TiledObjectUtil.createItinerary(map.getLayers().get("itinerary").getObjects());
        for (Itinerary.Checkpoint checkpoint : itinerary.getCheckpoints()) {
            addCheckpoint(checkpoint);
        }
        TiledObjectUtil.parseFrictionsLayer(world, map.getLayers().get("friction").getObjects());
        final Rectangle startRect = TiledObjectUtil.getRectangle(map.getLayers().get("start").getObjects(), "startSquare");
        float posX = startRect.x + startRect.width - 94;
        float posY = startRect.y + (startRect.height - 60);
        int i = 0;
        for (final Player player : players) {
            addKart(
                    posX * FlickKarts.UNIT_SCALE - Kart.RADIUS,
                    (posY - 224 * (i % 2)) * FlickKarts.UNIT_SCALE - Kart.RADIUS,
                    player
            );
            posX -= 256;
            i++;
        }
        //en la primera versio del joc no posem items, per tant deshabilitem el metode per generar les caixes
//        popItemBoxes();
        setCurrentKart(parameters.initialPlayer);
    }

    private void popItemBoxes() {
        for (Itinerary.Checkpoint checkpoint : itinerary.getCheckpoints()) {
            itemBoxes.add(new ItemBox(checkpoint.rectangle.x + (checkpoint.rectangle.width / 2),
                    checkpoint.rectangle.y + (checkpoint.rectangle.height / 2), world, this));
        }
    }

    public ArrayList<ItemBox> getItemBoxes() {
        return itemBoxes;
    }

    private void createBoundary() {
        final BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.StaticBody;
        Body body = world.createBody(def);

        Vector2[] worldVertices = new Vector2[]{
                new Vector2(boundaryBox.x, boundaryBox.y),
                new Vector2(boundaryBox.x + boundaryBox.width, boundaryBox.y),
                new Vector2(boundaryBox.x + boundaryBox.width, boundaryBox.y + boundaryBox.height),
                new Vector2(boundaryBox.x, boundaryBox.y + boundaryBox.height),
                new Vector2(boundaryBox.x, boundaryBox.y),
        };
        ChainShape shape = new ChainShape();
        shape.createChain(worldVertices);
        final FixtureDef fixture = new FixtureDef();
        fixture.shape = shape;
        body.createFixture(fixture);
        shape.dispose();
    }

    private Rectangle getMapBoundaryBox() {
        Rectangle worldSize = new Rectangle();
        for (MapLayer layer : map.getLayers()) {
            if (layer instanceof TiledMapTileLayer) {
                TiledMapTileLayer tiles = (TiledMapTileLayer) layer;
                worldSize.merge(new Rectangle(
                        layer.getRenderOffsetX() * FlickKarts.UNIT_SCALE, -layer.getRenderOffsetY() * FlickKarts.UNIT_SCALE,
                        tiles.getWidth() * tiles.getTileWidth() * FlickKarts.UNIT_SCALE, tiles.getHeight() * tiles.getTileHeight() * FlickKarts.UNIT_SCALE
                ));
            }
        }
        return worldSize;
    }

    public float getMapWidth() {
        return boundaryBox.width;
    }

    public float getMapHeight() {
        return boundaryBox.height;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    private void setFinishLine(final Rectangle finishLine) {
        addRectangleSensor(finishLine, this);
    }

    private void addCheckpoint(final Itinerary.Checkpoint checkpoint) {
        addRectangleSensor(checkpoint.rectangle, checkpoint);
    }

    private void addRectangleSensor(final Rectangle rect, final Object userData) {
        BodyDef def = new BodyDef();
        def.position.set(rect.x + rect.width / 2f, rect.y + rect.height / 2f);
        def.type = BodyDef.BodyType.StaticBody;
        Body body = world.createBody(def);
        body.setUserData(userData);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(rect.width / 2f, rect.height / 2f);

        final FixtureDef fixture = new FixtureDef();
        fixture.shape = shape;
        fixture.isSensor = true;

        body.createFixture(fixture);
        shape.dispose();
    }

    public Kart findKartById(int id) {
        for (Kart k : karts) {
            if (k.getId() == id) return k;
        }
        return null;
    }

    @Override
    public void dispose() {
        world.dispose();
        map.dispose();
        if (sfxPlayer != null) {
            sfxPlayer.dispose();
        }
    }

    public void update() {
        world.step(1 / 60f, 6, 2);
        for (ItemBox box : itemBoxesToDelete) {
            world.destroyBody(box.getBody());
            itemBoxes.remove(box);
        }
        itemBoxesToDelete.clear();
    }

    public void addKart(float posX, float posY, Player player) {
        Kart kart = new Kart(posX, posY, player, world);
        karts.add(kart);
    }

    public boolean isCurrentKart(int id) {
        return currentKart != null && currentKart.getId() == id;
    }

    public boolean flickFinished() {
        for (Kart kart : karts) {
            if (kart.isMoving()) return false;
        }
        return true;
    }

    public boolean isFinished() {
        return (kartsClassified.isEmpty() && karts.size() < 2)
                || (!kartsClassified.isEmpty() && karts.isEmpty());
    }

    public List<Kart> getKarts() {
        return karts;
    }

    public void flick(int playerID, Vector2 impulse) {
        findKartById(playerID).flick(impulse);
    }

    public boolean isTouchingKart(Kart kart, Vector2 touchPoint) {
        isOverKart = false;
        if (kart != null) {
            queryTouchPoint.set(touchPoint);
            queryKart = kart;
            world.QueryAABB(this, touchPoint.x, touchPoint.y, touchPoint.x, touchPoint.y);
        }
        return isOverKart;
    }

    @Override
    public boolean reportFixture(Fixture fixture) {
        if (queryKart.itsYours(fixture) && fixture.testPoint(queryTouchPoint)) {
            isOverKart = true;
            return false;
        }
        return true;
    }

    public World getWorld() {
        return world;
    }

    public TiledMap getMap() {
        return map;
    }

    public int getCurrentId() {
        return currentKart.getId();
    }

    private boolean chooseNextPlayer() {
        for (Kart kart : karts) {
            if (kart.getCurrentRound() == currentRound) continue;
            currentKart = kart;
            return true;
        }
        return false;
    }

    public void adjustKart(int id, Vector2 position, float angle, int sector, int lap) {
        Kart kart = findKartById(id);
        if (kart != null) kart.adjust(position, angle, sector, lap);
    }

    public void nextTurn(List<Kart> classified) {
        currentKart.setCurrentRound(currentRound);
        for (Kart kart : kartsToClassify) {
            classifyKart(kart.getId());
            classified.add(kart);
        }
        kartsToClassify.clear();
        if (!kartsClassified.contains(currentKart) && currentKart.hasBurst()) {
            currentKart.removeBurst();
            return; //return per no cridar el chooseNextPlayer i continuar amb el mateix currentKart
        }
        if (!karts.isEmpty() && !chooseNextPlayer()) {
            currentRound++;
            log.debug("Server round: {}", currentRound);
            sortKarts();
            currentKart = karts.get(0);
        }
    }

    public void classifyKart(int id) {
        kartsClassified.add(findKartById(id));
        removeKart(id);
    }

    private void sortKarts() {
        Collections.sort(karts, new Comparator<Kart>() {
            @Override
            public int compare(Kart o1, Kart o2) {
                if (o1.getLap() != o2.getLap()) return o2.getLap() - o1.getLap();
                int closestNodeO1 = itinerary.findClosestNode(o1.getPosition());
                int closestNodeO2 = itinerary.findClosestNode(o2.getPosition());
                if (closestNodeO1 == closestNodeO2) {
                    float distanceO1 = itinerary.distanceToNode(o1.getPosition(), closestNodeO1 + 1);
                    float distanceO2 = itinerary.distanceToNode(o2.getPosition(), closestNodeO2 + 1);
                    return Float.compare(distanceO2, distanceO1);
                }
                return closestNodeO2 - closestNodeO1;
            }
        });
    }

    public void crossedFinishLine(Kart kart) {
        Vector2 kartDirection = kart.getDirection();
        Vector2 itineraryDirection = itinerary.getDirection(itinerary.findClosestNode(kart.getPosition()));
        double angle = Math.atan2(kartDirection.y - itineraryDirection.y, kartDirection.x - itineraryDirection.x) * MathUtils.radiansToDegrees;
        if (angle <= 90 && angle >= -90 && kart.getCurrentSector() >= 3) {
            kart.crossFinishLine();
            if (sfxPlayer != null) {
                sfxPlayer.playSfxLap();
            }
            if (kart.getLap() == laps) {
                kartsToClassify.add(kart);
            }
        }
    }

    public Kart getCurrentKart() {
        return currentKart;
    }

    public void setCurrentKart(Kart currentKart) {
        this.currentKart = currentKart;
    }

    public void setCurrentKart(int id) {
        setCurrentKart(findKartById(id));
    }

    public boolean removeKart(int id) {
        Kart kart = findKartById(id);
        if (kart == null) return false;
        karts.remove(kart);
        kart.dispose();
        return kart == currentKart;
    }

    public List<Kart> getClassification() {
        ArrayList<Kart> classification = new ArrayList<Kart>(kartsClassified);
        classification.addAll(karts);
        return classification;
    }

    public void deleteItemBox(ItemBox itemBox) {
        itemBoxesToDelete.add(itemBox);
    }

    public void generateRandomItem(Kart kart) {
        listener.generateRandomItem(kart);
    }

    public void kartUseItem(int id) {
        Kart kart = findKartById(id);
        if (kart != null) {
            kart.useItem();
        }
    }

    public interface Listener {
        void generateRandomItem(Kart kart);
    }

    public static class Parameters {
        /**
         * Number of laps required to run the race.
         **/
        public int laps = 4;
        /**
         * The name of the file to load for track info
         **/
        public String track = "track2.tmx";
        /**
         * The ID of the player that will flick first.
         **/
        public int initialPlayer = 0;

        @Override
        public String toString() {
            return "Parameters{" +
                    "laps=" + laps +
                    ", track='" + track + '\'' +
                    ", initialPlayer=" + initialPlayer +
                    '}';
        }
    }
}