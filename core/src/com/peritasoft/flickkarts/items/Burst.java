package com.peritasoft.flickkarts.items;


import com.peritasoft.flickkarts.Kart;

public class Burst implements Item{

    @Override
    public ItemIcon getIcon() {
        return ItemIcon.BURST;
    }

    @Override
    public void use(Kart kart) {
        kart.setBurst();
        System.out.println("BURST!!");
    }
}
