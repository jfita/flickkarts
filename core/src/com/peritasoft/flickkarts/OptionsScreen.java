package com.peritasoft.flickkarts;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;

public class OptionsScreen extends FlickKartsScreen {

    private ClientScreen clientScreen;
    private MusicPlayer musicPlayer;

    public OptionsScreen(FlickKarts game, ClientScreen clientScreen, MusicPlayer musicPlayer) {
        super(game);
        this.clientScreen = clientScreen;
        this.musicPlayer = musicPlayer;
        setupLayout(game.getSkin());
    }

    private void setupLayout(Skin skin) {
        final Table table = new Table(skin);
        table.setFillParent(true);
        addActor(table);

        final Dialog dialog = new Dialog("", skin);
        dialog.setMovable(false);

        final TextButton leaveRace = new TextButton("Leave Race", skin);
        leaveRace.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                leaveRace();
            }
        });
        dialog.getButtonTable().add(leaveRace).fillX().expandX();
        dialog.getButtonTable().row();

        String buttonLabel = game.isMuted() ? "Unmute sound" : "Mute sound";
        final TextButton muteUnmute = new TextButton(buttonLabel, skin);
        muteUnmute.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                musicPlayer.muteUnmute();
            }
        });
        dialog.getButtonTable().add(muteUnmute).fillX().expandX().padTop(20f);
        dialog.getButtonTable().row();

        final TextButton resume = new TextButton("Resume", skin);
        resume.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                goBack();
            }
        });
        dialog.getButtonTable().add(resume).fillX().expandX().padTop(20f);
        table.add(dialog).center();
    }

    @Override
    public void draw() {
        clientScreen.draw();
        super.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        clientScreen.resize(width, height);
    }

    @Override
    protected void goBack() {
        super.goBack();
        final ClientScreen next = clientScreen;
        clientScreen = null;
        game.replaceScreen(next);
    }

    private void leaveRace() {
        game.replaceScreen(new MenuScreen(game, null));
    }

    @Override
    public void dispose() {
        super.dispose();
        if (clientScreen != null) {
            clientScreen.dispose();
        }
    }
}
